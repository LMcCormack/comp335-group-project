class Server {

    private String type;
    private int limit;
    private int bootupTime;
    private float rate;
    private int coreCount;
    private int memory;
    private int disk;

    public Server(String type, int limit, int bootupTime, float rate, int coreCount, int memory, int disk) {
        this.type = type;
        this.limit = limit;
        this.bootupTime = bootupTime;
        this.rate = rate;
        this.coreCount = coreCount;
        this.memory = memory;
        this.disk = disk;
    }

    @Override
    public String toString() {
        return "<" + type + ", " + limit + ", " + bootupTime + ", " + rate + ", "
                + coreCount + ", " + memory + ", " + disk + ">";
    }

    public String getType()
    {
        return this.type;
    }

    public int getLimit()
    {
        return this.limit;
    }

    public int getBootupTime()
    {
        return this.bootupTime;
    }

    public float getRate()
    {
        return this.rate;
    }

    public int getCoreCount()
    {
        return this.coreCount;
    }

    public int getMemorString()
    {
        return this.memory;
    }

    public int getDisk()
    {
        return this.disk;
    }
}
