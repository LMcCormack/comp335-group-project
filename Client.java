/* COMP335 Distributed System Simulation Project
TEAM MEMBERS: Kelvin Liu, Julian Cesaro, Lachlan McCormack

*/

import java.io.*;
import java.net.*;
import java.util.*;

public class Client {
    // Declare socket, input stream, and output stream
    private Socket socket = null;
    private InputStream is = null;
    private DataOutputStream out = null;

    // System Server Information
    String serverInfo = "";
    String[] servers = new String[7];
    int jobID = 0;
    public static ArrayList<Server> serverList = new ArrayList<Server>();

    // String that will hold messages and message buffer
    String line = "";
    byte[] msg = null;

    // Client constructor: creates socket and authenticates
    public Client(String address, int port) {
        createSocket(address, port);
    }

    // Creates the socket connection and output channel
    public void createSocket(String address, int port) {
        try {
            this.socket = new Socket(address, port);

            System.out.println("Connected");

            this.out = new DataOutputStream(this.socket.getOutputStream());
        } catch (UnknownHostException u) {
            System.out.println(u);
        } catch (IOException i) {
            System.out.println(i);
        }
    }

    // Function to receive messages from the server
    // Returns: string
    public String receiveMsg() {
        try {
            is = socket.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String message = br.readLine();
            return message;
        } catch (IOException i) {
            System.out.println(i);
            return null;
        }
    }

    public void sendMsg(String m) {
        try {
            line = m + "\n";
            msg = line.getBytes();
            out.write(msg);
        } catch (IOException i) {
            System.out.println(i);
        }
    }

    public void authenticate() {
        String user = System.getProperty("user.name");

        // Create a message and send to server
        sendMsg("HELO");

        // Receive message from server
        line = receiveMsg();

        sendMsg("AUTH " + user);

        line = receiveMsg();

        //readXML("/home/comp335/comp335/ds-sim/system.xml");
    }


    public boolean fits(String[] server, String[] job) {
        //job [3] = time, [4] = cores, [5] = mem, [6] = disk
        //server [3] = time, [4] = cores, [5] = mem, [6] = disk


        //check if all metrics fit.
        for (int i = 4; i < job.length - 1; i++) {
			if (Integer.valueOf(job[i]) > Integer.valueOf(server[i])) {

				return false;
			}

		}
        return true;

    }


	class sortByCores implements Comparator<String[]> {
		public int compare(String[] a, String[] b) {
			//servers[i][4] is the number of cores of server i
			return Integer.valueOf(a[4]) - Integer.valueOf(b[4]);
		}

	}



	public void firstFit() {
        String jobInfo = "", worstType = "", altType = "";
        String[] job = new String[7];

        ArrayList<String[]> servers = new ArrayList<String[]>();
        ArrayList<String[]> firstServ = new ArrayList<String[]>();
        boolean sorted = false, firstRun = false, schd = false;

        while (!line.equals("QUIT\n")) {
            sendMsg("REDY");
            //receive message from server
            jobInfo = receiveMsg();
            job = jobInfo.split(" ");

            if (!jobInfo.equals("NONE")) {

                sendMsg("RESC All");
                // ArrayList<String[]> servers = new ArrayList<String[]>();
                ArrayList<String[]> serverUpdate = new ArrayList<String[]>();
                serverInfo = receiveMsg();
                schd = false;

                while (!serverInfo.equals(".")) {

                    sendMsg("OK");

                    serverInfo = receiveMsg();
                    //Only add server info to array of server if it is not "." or "DATA"
                    if (!serverInfo.equals(".") && !serverInfo.equals("DATA")) {
                        serverUpdate.add(serverInfo.split(" "));
                    }
                    if (serverInfo.equals(".")) {
                        break;
                    }
                }

                //Sort servers by cores if first run through;
                if (sorted == false) {
                    Collections.sort(serverUpdate, new sortByCores());
                    sorted = true;
                    servers = serverUpdate;
                    if(firstRun == false){
                        for(int i = 0; i < servers.size(); i++){
                            firstServ.add(servers.get(i));
                        }
                    }
                    firstRun = true;
                } else {
                    //Update servers
                    for (int i = 0; i < servers.size(); i++) {
                        for (int x = 0; i < serverUpdate.size(); x++) {
                            if (servers.get(i)[0].equals(serverUpdate.get(x)[0]) && servers.get(i)[1].equals(serverUpdate.get(x)[1])) {
                                String[] curr = serverUpdate.get(x);
                                servers.set(i, curr);
                                break;
                            }
                        }
                    }
                }

                //go through arraylist of servers and check for fitness.
                for (int k = 0; k < servers.size(); k++) {
                    if (fits(servers.get(k), job)) {
                        //if fit send SCHD message
                        sendMsg("SCHD " + job[2] + " " + servers.get(k)[0] + " " + servers.get(k)[1]);
                        receiveMsg();
                        schd = true;
                        break;
                    }
                }
                if(schd == false){
                    for (int k = 0; k < firstServ.size(); k++) {
                        if (fits(firstServ.get(k), job) && Integer.parseInt(servers.get(k)[2]) == 3) {
                            //if fit send SCHD message
                            sendMsg("SCHD " + job[2] + " " + servers.get(k)[0] + " " + servers.get(k)[1]);
                            receiveMsg();
                            break;
                        }
                    }
                }
            } else {
                close();
            }
        }
    }



    // Main loop of the Client
    public void allToLargest() {

        boolean largestServerCheck = false;
        String largestServerType = "";
        int largestCoreCount = 0;

        while (!line.equals("QUIT\n")) {

            sendMsg("REDY");
            //receive message from server
            line = receiveMsg();
            System.out.println(line);

            if (largestServerCheck == false) {

                sendMsg("RESC All");

                while (!serverInfo.equals(".")) {

                    sendMsg("OK");

                    serverInfo = receiveMsg();

                    //Only add server info to array of server if it is not "." or "DATA"
                    if (!serverInfo.equals(".") && !serverInfo.equals("DATA")) {

                        servers = serverInfo.split(" ");

                        System.out.println(servers[2]);

                        //If current coreCount is greater than largest coreCount
                        //Make current server the largestServer
                        //Server type is at servers[0] and core count is at servers[4]
                        if (Integer.parseInt(servers[4]) > largestCoreCount) {
                            largestCoreCount = Integer.parseInt(servers[4]);
                            largestServerType = servers[0];
                        }
                    }
                    if (serverInfo.equals(".")) {
                        break;
                    }
                }
                largestServerCheck = true;
            }
            if (!line.equals("NONE")) {

                sendMsg("SCHD " + jobID + " " + largestServerType + " 0");

                jobID++;

                //receive message from server
                line = receiveMsg();

            } else if (line.equals("NONE")) {

                close();
            }
        }
    }

    public void worst_Fit() {
        String jobInfo = "", worstType = "", altType = "", firstType = "";
        String[] job = new String[7];
        int worstID = 0, altID = 0;
        int jobCores = 0;
        int worstFit = Integer.MIN_VALUE;
        int altFit = Integer.MIN_VALUE;
        boolean fallback = false;
        boolean largeActive = false;
        int firstID = 0;
        int elseFit = Integer.MIN_VALUE;

        while (!line.equals("QUIT\n")) {

            sendMsg("REDY");
            //receive message from server
            jobInfo = receiveMsg();
            job = jobInfo.split(" ");

            if (!jobInfo.equals("NONE")) {

                worstFit = Integer.MIN_VALUE;
                altFit = Integer.MIN_VALUE;

                sendMsg("RESC All");

                serverInfo = receiveMsg();

                while (!serverInfo.equals(".")) {

                    sendMsg("OK");

                    serverInfo = receiveMsg();

                    //Only add server info to array of server if it is not "." or "DATA"
                    if (!serverInfo.equals(".") && !serverInfo.equals("DATA")) {

                        servers = serverInfo.split(" ");

                        //If current coreCount is greater than largest coreCount
                        //Make current server the largestServer
                        //Server type is at servers[0] and core count is at servers[4]
                        int serverState = Integer.parseInt(servers[2]);
                        int fitnessValue = 0;
                        if (Integer.parseInt(servers[4]) >= Integer.parseInt(job[4]) &&
                                (serverState == 2 || serverState == 0 || serverState == 3)) {
                            fitnessValue = Integer.parseInt(servers[4]) - Integer.parseInt(job[4]);
                            if (fitnessValue > worstFit && (Integer.parseInt(servers[3]) == Integer.parseInt(job[1]) ||
                                    Integer.parseInt(servers[3]) == -1)) {
                                worstFit = fitnessValue;
                                worstType = servers[0];
                                worstID = Integer.parseInt(servers[1]);
                            } else if (fitnessValue > altFit && (serverState == 0 ||
                                    serverState == 2 || serverState == 3)) {
                                altFit = fitnessValue;
                                altType = servers[0];
                                altID = Integer.parseInt(servers[1]);
                            }
                        }
                        if(fallback == false && fitnessValue > elseFit){
                            firstType = servers[0];
                        }
                        if(servers[0].equals(firstType) && serverState == 3 && largeActive == false){
                            firstID = Integer.parseInt(servers[1]);
                            largeActive = true;
                        }
                    }
                    if (serverInfo.equals(".")) {
                        fallback = true;
                        break;
                    }
                }

                if (worstFit > Integer.MIN_VALUE) {
                    sendMsg("SCHD " + job[2] + " " + worstType + " " + worstID);
                    line = receiveMsg();
                }
                else if (altFit > Integer.MIN_VALUE) {
                    sendMsg("SCHD " + job[2] + " " + altType + " " + altID);
                    line = receiveMsg();
                } else {
                    sendMsg("SCHD " + job[2] + " " + firstType + " " + firstID);
                    line = receiveMsg();
                    largeActive = false;
                }
            }
            else if (jobInfo.equals("NONE")) {

                close();
            }
        }
    }

    public void bestFit()
    {
        String jobInfo = "", bestType = "", firstType = "";
        int bestID = 0, firstID = 0;
        int bestFit = Integer.MAX_VALUE;
        int minAvail = Integer.MAX_VALUE;
        int bestFitElse = Integer.MAX_VALUE;
        int minAvailElse = Integer.MAX_VALUE;
        int fitness = 0;
        boolean bestFitFound = false;
        String[] job = new String[7];
        
        //Flag to indicate first iteration of resc all
        boolean first = false;

        //Store all of the servers on the first run through RESC all
        //Used to gather intial resource capacity
        ArrayList<String[]> initialServer = new ArrayList<String[]>();
        int iterator = 0;

        while(!line.equals("QUIT\n"))
        {
            //Get a JOB
            sendMsg("REDY");
            jobInfo = receiveMsg();
            //System.out.println(jobInfo);
            job = jobInfo.split(" ");
            
            //Process each job
            if(!jobInfo.equals("NONE"))
            {
                bestFit = Integer.MAX_VALUE;
                minAvail = Integer.MAX_VALUE;
                bestFitElse = Integer.MAX_VALUE;
                minAvailElse = Integer.MAX_VALUE;
                
                sendMsg("RESC All");
                serverInfo = receiveMsg();

                //Process each server until . is reached
                while(!serverInfo.equals("."))
                {
                    sendMsg("OK");
                    serverInfo = receiveMsg();

                    if (!serverInfo.equals(".") && !serverInfo.equals("DATA"))
                    {
                        //Split server information into elements
                        servers = serverInfo.split(" ");

                        if(first == false){
                            initialServer.add(servers);
                        }

                        fitness = 0;

                        //Compare server cores to cores required by job
                        if(Integer.parseInt(servers[4]) >= Integer.parseInt(job[4]))
                        {
                            //Calculate fitness
                            fitness = Integer.parseInt(servers[4]) - Integer.parseInt(job[4]);
                            
                            //If server is a better fit and has the available time
                            if(fitness < bestFit || (fitness == bestFit && Integer.parseInt(servers[3]) < minAvail))
                            {
                                bestFitFound = true;
                                bestFit = fitness;
                                bestType = servers[0];
                                bestID = Integer.parseInt(servers[1]);
                                minAvail = Integer.parseInt(servers[3]);
                            }
                        }
                        else {
                            //Calculate fitness
                            fitness = Integer.parseInt(initialServer.get(iterator)[4]) - Integer.parseInt(job[4]);

                            //If server is a better fit and has the available time
                            if (fitness >= 0) {
                                if(Integer.parseInt(servers[2]) == 3 && (fitness < bestFitElse)) {
                                    bestFitElse = fitness;

                                    //Get firstType and firstID from the intial servers (indicating intial capacity)
                                    firstType = initialServer.get(iterator)[0];
                                    firstID = Integer.parseInt(initialServer.get(iterator)[1]);
                                    minAvailElse = Integer.parseInt(servers[3]);
                                }
                            }
                            iterator++;
                        }
                    }
                }
                //Reset iterator and first back to   
                iterator = 0;
                first = true;

                if(bestFitFound)
                {
                    sendMsg("SCHD " + job[2] + " " + bestType + " " + bestID);
                    line = receiveMsg();
                    bestFitFound = false;
                }
                else
                {
                    //Best fit was not found so schedule to 
                    sendMsg("SCHD " + job[2] + " " + firstType + " " + firstID);
                    line = receiveMsg();
                }
            }
            else if(jobInfo.equals("NONE"))
            {
                close();
            }
        }
    }


    public void readXML(String file){
        DOMParser p = new DOMParser(file);

        serverList = p.serverArr;
    }

    // Function to terminate connection
    public void close() {
        sendMsg("QUIT");
    }

    public static void main(String args[]) {
        Client client = new Client("127.0.0.1", 8096);
        client.authenticate();
        if(args.length > 1) {
            if (args[0].equals("-a")) {
                if (args[1].equals("ff")) {
                    //use first fit
					client.firstFit();
                    System.out.println("first fit");
                } else if (args[1].equals("bf")) {
                    //use best fit
                    client.bestFit();
                    System.out.println("best fit");
                } else if (args[1].equals("wf")) {
                    //use worst fit
                    client.worst_Fit();
                    System.out.println("worst fit");
                }
            }
        }
        else if(args.length >= 1) {
            System.out.println("Client: option requires an argument -- 'a'\n" +
                    "Option -a requires an argument.\n" +
                    "Usage:\n" +
                    "       java Client [-a algo_name(ff/bf/wf)]");
        }
        else {
            client.allToLargest();
            System.out.println("allto");
        }
        client.close();
    }
}
