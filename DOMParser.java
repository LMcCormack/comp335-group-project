import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import java.util.ArrayList;

public class DOMParser {

    public static ArrayList<Server> serverArr = new ArrayList<Server>();

    public DOMParser(String file) {

        if (file == null)
            throw new RuntimeException("The name of the XML file is required!");

        try {

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            Document document = builder.parse(new File(file));

            NodeList nodeList = document.getDocumentElement().getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE) {

                    NodeList serverList = node.getChildNodes();

                    for (int k = 0; k < serverList.getLength(); k++) {

                        Node serverInfo = serverList.item(k);

                        if (serverInfo.getNodeType() == Node.ELEMENT_NODE) {

                            Element elem = (Element) serverInfo;

                            String type = serverInfo.getAttributes().getNamedItem("type").getNodeValue();

                            Integer limit = Integer.parseInt(serverInfo.getAttributes()
                                    .getNamedItem("limit").getNodeValue());

                            Integer bootupTime = Integer.parseInt(serverInfo.getAttributes()
                                    .getNamedItem("bootupTime").getNodeValue());

                            Float rate = Float.parseFloat(serverInfo.getAttributes()
                                    .getNamedItem("rate").getNodeValue());

                            Integer coreCount = Integer.parseInt(serverInfo.getAttributes()
                                    .getNamedItem("coreCount").getNodeValue());

                            Integer memory = Integer.parseInt(serverInfo.getAttributes()
                                    .getNamedItem("memory").getNodeValue());

                            Integer disk = Integer.parseInt(serverInfo.getAttributes()
                                    .getNamedItem("disk").getNodeValue());


                            serverArr.add(new Server(type, limit, bootupTime, rate, coreCount, memory, disk));
                        }
                    }
                }
            }
        }
        catch (ParserConfigurationException | SAXException | IOException e){
            System.out.println(e.getMessage());
        }
    }
}
